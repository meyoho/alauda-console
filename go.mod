module bitbucket.org/mathildetech/alauda-console

go 1.12

require (
	bitbucket.org/mathildetech/alauda-backend v0.1.24
	bitbucket.org/mathildetech/app v1.0.1
	bitbucket.org/mathildetech/log v1.0.5
	github.com/coreos/go-oidc v2.0.0+incompatible
	github.com/emicklei/go-restful v2.9.6+incompatible
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.3.2
	go.uber.org/zap v1.9.1
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	gopkg.in/square/go-jose.v2 v2.3.1 // indirect
	k8s.io/apimachinery v0.0.0-20190313205120-d7deff9243b1
	k8s.io/client-go v11.0.0+incompatible
)
