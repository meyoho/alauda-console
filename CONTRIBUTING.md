# Contributing

## Tooling

- go >= 1.13
- [gobindata](https://github.com/jteeuwen/go-bindata)
- Make
- docker

## Structure

- `build`: used to store make libraries, dockerfiles, linters, etc,
- `cmd`: application main entrypoint. Will use to build the binary
- `pkg`: library/packages to encapsulate the core reusable code
- `test`: files and code used for testing

## Workflow

Check [`Makefile`](Makefile) for all available commands

## Libraries

[WIP] Check [go.mod](go.mod) for more