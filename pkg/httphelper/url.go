package httphelper

import (
	"net/http"
	"net/url"
)

const (
	refererHeader = "Referer"
	// RefererHostEmpty means parse host field from referer header failed.
	RefererHostEmpty = RefererGetErr("cannot parse host from referer")
	// RefererSchemeEmpty means parse scheme field from referer header failed.
	RefererSchemeEmpty = RefererGetErr("cannot parse scheme from referer")
)

// RefererGetErr is an error type means parse referer failed
type RefererGetErr string

func (e RefererGetErr) Error() string {
	return string(e)
}

// GetReferer returns referer's scheme and host from request
func GetReferer(req *http.Request) (scheme, host string, err error) {
	referer := req.Header.Get(refererHeader)
	u, err := url.Parse(referer)
	if err != nil {
		return "", "", err
	}
	if u.Scheme == "" {
		return "", "", RefererSchemeEmpty
	}
	if u.Host == "" {
		return "", "", RefererHostEmpty
	}
	return u.Scheme, u.Host, nil
}
