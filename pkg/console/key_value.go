package console

// KeyValue key value struct
type KeyValue struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}
