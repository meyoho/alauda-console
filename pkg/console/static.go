package console

import (
	"net/http"
	"os"
	"path"
	"strings"

	"bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/alauda-console/pkg/httphelper"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"go.uber.org/zap"
)

// Console conole data
type Console struct {
	APIAddress        string     `json:"apiAddress"`
	AssetsHostPath    string     `json:"assetsHostPath"`
	AssetsURLPath     string     `json:"assetsURLPath"`
	CustomEnvVars     []KeyValue `json:"environment"`
	DisableDynamicURL bool       `json:"-"`
}

var logger *zap.Logger

// New creates new instance
func New() *Console {
	return &Console{
		CustomEnvVars: []KeyValue{},
	}
}

// ApplyToServer apply to server
func (c *Console) ApplyToServer(srv server.Server) error {
	gen := decorator.NewWSGenerator()
	ws := gen.New(srv)
	logger = srv.L().Named("console")
	ws.Doc("Basic APIs for alauda-console")
	ws.Path("/api/v1")
	ws.ApiVersion("v1")

	ws.Route(
		ws.GET("modules").
			To(c.HandleModules).
			Doc("Fetch console information about modules").
			Writes(Console{}).
			Returns(200, "OK", Console{}),
	)

	ws.Route(
		ws.GET("envs").
			To(c.HandleEnvVars).
			Doc("Fetch environment variables from system").
			Writes(EnvVars{}).
			Returns(200, "OK", EnvVars{}),
	)

	assetsURLPath := c.AssetsURLPath
	logger.Info("will serve static files on " + assetsURLPath)
	assetsURLPath = strings.TrimSuffix(assetsURLPath, "/")

	srv.Container().Add(ws)

	ws = new(restful.WebService)
	ws.Route(
		ws.GET(assetsURLPath + "/").
			Doc("Static file serving").
			To(c.HandleStaticFiles),
	)
	ws.Route(
		ws.GET(assetsURLPath + "/{subpath:*}").
			Doc("Static file serving").
			To(c.HandleStaticFiles),
	)
	srv.Container().Add(ws)
	return nil
}

// HandleModules handle modules api
func (c *Console) HandleModules(req *restful.Request, res *restful.Response) {
	scheme, host, err := httphelper.GetReferer(req.Request)
	if err != nil || c.DisableDynamicURL {
		res.WriteAsJson(c)
		return
	}
	res.WriteAsJson(&Console{
		APIAddress:     scheme + "://" + host,
		AssetsHostPath: c.AssetsHostPath,
		AssetsURLPath:  c.AssetsURLPath,
		CustomEnvVars:  c.CustomEnvVars,
	})
}

// HandleStaticFiles handle static files serving
func (c *Console) HandleStaticFiles(req *restful.Request, res *restful.Response) {
	subpath := req.PathParameter("subpath")
	if subpath == "" {
		subpath = "index.html"
	}
	actual := path.Join(c.AssetsHostPath, subpath)

	// in order to support HTML5 routing for single page applications
	// we need to return the index.html file by default if the file was not found
	_, err := os.Lstat(actual)
	if err != nil && os.IsNotExist(err) {
		newRedirect := path.Join(c.AssetsHostPath, "index.html")
		logger.Warn("file not found, returning index...", log.String("file", actual), log.String("index", newRedirect))
		actual = newRedirect
	}
	logger.Debug("serving file", log.String("file", actual))
	http.ServeFile(
		res.ResponseWriter,
		req.Request,
		actual)
}

// HandleEnvVars return os environment variables as EnvVars struct
func (c *Console) HandleEnvVars(req *restful.Request, res *restful.Response) {
	envs := os.Environ()
	envMap := EnvVars{}
	for _, env := range envs {
		values := strings.SplitN(env, "=", 2)
		if len(values) == 2 {
			envMap[values[0]] = values[1]
		} else {
			logger.Debug("Env variable invalid", log.String("env", env), log.Any("splitted", values))
		}
	}
	if len(c.CustomEnvVars) > 0 {
		for _, item := range c.CustomEnvVars {
			envMap[item.Key] = item.Value
		}
	}
	res.WriteAsJson(envMap)
}

// EnvVars environment variables
type EnvVars map[string]string
