package oidc

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net"
	"strings"
	"sync"

	"bitbucket.org/mathildetech/log"

	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"math/rand"

	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/alauda-console/pkg/httphelper"
	gooidc "github.com/coreos/go-oidc"
	"github.com/emicklei/go-restful"
	"go.uber.org/zap"
	"golang.org/x/net/http/httpproxy"
	"golang.org/x/oauth2"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var logger *zap.Logger

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

// randomString generates a random string with given length
func randomString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// Config configuration items for OIDC authentication flow
type Config struct {
	Config     *oauth2.Config
	Provider   *gooidc.Provider
	Verifier   *gooidc.IDTokenVerifier
	HTTPClient *http.Client
	Options    []oauth2.AuthCodeOption
}

// IsComplete returns true if all configuration items are initiated
func (o *Config) IsComplete() bool {
	return o.Config != nil && o.Provider != nil && o.HTTPClient != nil && o.Options != nil
}

// Handler main OIDC handler struct
type Handler struct {
	CAFile         string
	ClientID       string
	IssuerURL      string
	UsernameClaim  string
	UsernamePrefix string
	GroupsClaim    string
	GroupsPrefix   string
	TenantIDClaim  string
	TenantIDPrefix string
	ResponseType   string
	Nonce          string
	State          string
	SigningAlgs    []string
	RequiredClaims map[string]string
	ClientSecret   string
	RedirectURL    string
	Timeout        time.Duration
	Scopes         []string
	// ProtocolOverride if provided will replace the protocol returned by the provider
	// on the Login method
	ProtocolOverride string

	HTTPProxy     *httpproxy.Config
	proxyLock     sync.Once
	httpProxyFunc func(*http.Request) (*url.URL, error)

	Server server.Server

	lock            *sync.RWMutex
	config          *Config
	cacheExpireTime time.Time
	CacheDuration   time.Duration

	// Whether to disable dynamic url
	DisableDynamicURL bool
}

// NewHandler creates a new handler with the necessary data
func NewHandler() *Handler {
	return &Handler{
		Nonce:           randomString(8),
		State:           randomString(8),
		lock:            &sync.RWMutex{},
		cacheExpireTime: time.Now(),
	}
}

// ApplyToServer apply configuration to server
func (h *Handler) ApplyToServer(srv server.Server) error {
	h.Server = srv

	ws := new(restful.WebService).Consumes(restful.MIME_JSON).Produces(restful.MIME_JSON)

	logger = srv.L().Named("oidc")
	ws.Path("/api/v1/token")
	ws.Doc("OIDC authentication related APIs")

	ws.Route(
		ws.GET("login").
			To(h.HandleLogin).
			Doc("Fetch authorization url for OIDC login").
			Writes(LoginInfo{}).
			Returns(http.StatusOK, "OK", LoginInfo{}).
			Returns(http.StatusBadRequest, "BadRequest", metav1.Status{}),
	)

	ws.Route(
		ws.GET("callback").
			To(h.HandleCallback).
			Doc("Callback for OIDC authentication using code or id_token to confirm login").
			Param(
				restful.QueryParameter("code", "authorization code for code response_type flow").
					DataType("string").Required(true),
			).
			Param(
				restful.QueryParameter("state", "state given by the authorization flow to the OIDC provider. Must be set to validate man in the middle attacks").
					DataType("string").Required(true),
			).
			Param(
				restful.QueryParameter("id_token", "id token for id_token response_type flow").
					DataType("string"),
			).
			Writes(UserInfo{}).
			Returns(http.StatusOK, "OK", UserInfo{}).
			Returns(http.StatusBadRequest, "BadRequest", metav1.Status{}).
			Returns(http.StatusUnauthorized, "Unauthorized", metav1.Status{}),
	)

	ws.Route(
		ws.GET("info").
			To(h.HandleInfo).
			Doc("Retrieves user information from a Bearer token in the authorization header").
			Param(
				restful.HeaderParameter("Authorization", "Bearer <token>").
					Required(true),
			).
			Writes(UserInfo{}).
			Returns(http.StatusOK, "OK", UserInfo{}).
			Returns(http.StatusUnauthorized, "Unauthorized", metav1.Status{}),
	)

	ws.Route(
		ws.GET("refresh").
			To(h.HandleRefresh).
			Doc("Refresh token API").
			Param(
				restful.QueryParameter("refresh_token", "refresh token for user").
					DataType("string").
					Required(true),
			).Writes(RenewPayload{}).
			Returns(http.StatusOK, "ok", RenewPayload{}).
			Returns(http.StatusBadRequest, "BadRequest", metav1.Status{}).
			Returns(http.StatusUnauthorized, "Unauthorized", metav1.Status{}),
	)

	srv.Container().Add(ws)
	return nil
}

// LoginInfo reponse for a login request
type LoginInfo struct {
	AuthURL string `json:"auth_url"`
	State   string `json:"state"`
}

// HandleLogin generates the auth url for login (OIDC flow)
func (h *Handler) HandleLogin(req *restful.Request, res *restful.Response) {
	config, err := h.genOIDCClient(req.Request)
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}

	data := LoginInfo{
		AuthURL: config.Config.AuthCodeURL(h.State, config.Options...),
		State:   h.State,
	}
	logger.Debug("generated auth url", log.String("url", data.AuthURL))
	res.WriteAsJson(data)
}

func (h *Handler) configOverride(req *http.Request, config *Config) *Config {
	newConfig := &Config{
		Provider:   config.Provider,
		Verifier:   config.Verifier,
		HTTPClient: config.HTTPClient,
		Options:    config.Options,
		Config: &oauth2.Config{
			ClientID:     config.Config.ClientID,
			ClientSecret: config.Config.ClientSecret,
			Endpoint: oauth2.Endpoint{
				AuthURL:   config.Config.Endpoint.AuthURL,
				TokenURL:  config.Config.Endpoint.TokenURL,
				AuthStyle: config.Config.Endpoint.AuthStyle,
			},
			RedirectURL: config.Config.RedirectURL,
			Scopes:      config.Config.Scopes,
		},
	}

	if h.DisableDynamicURL {
		if h.ProtocolOverride != "" {
			currentProtocol := strings.Split(newConfig.Config.Endpoint.AuthURL, "://")[0]
			if currentProtocol != h.ProtocolOverride {
				logger.Debug("replacing protocol auth protocol", log.String("current", currentProtocol), log.String("desired", h.ProtocolOverride))
				newConfig.Config.Endpoint.AuthURL = strings.Replace(newConfig.Config.Endpoint.AuthURL, fmt.Sprintf("%s://", currentProtocol), fmt.Sprintf("%s://", h.ProtocolOverride), 1)
			}
		}
		return newConfig
	}

	// get referer host and scheme
	scheme, host, err := httphelper.GetReferer(req)
	if err != nil {
		logger.Warn("error getting referer url: ", log.Err(err))
		return newConfig
	}

	// set dynamic redirect url
	u, err := url.Parse(h.RedirectURL)
	if err == nil {
		u.Scheme = scheme
		u.Host = host
		newConfig.Config.RedirectURL = u.String()
	} else {
		logger.Warn("error parsing redirect url: ", log.Err(err))
	}

	// set dynamic auth url
	u, err = url.Parse(config.Config.Endpoint.AuthURL)
	if err == nil {
		u.Scheme = scheme
		u.Host = host
		newConfig.Config.Endpoint.AuthURL = u.String()
	} else {
		logger.Error("error parsing auth url: ", log.Err(err))
	}

	return newConfig
}

// UserInfo defines a data structure containing user information.
type UserInfo struct {
	Name   string                 `json:"name"`
	Email  string                 `json:"email,omitempty"`
	UID    string                 `json:"uid"`
	Groups []string               `json:"groups"`
	Extra  map[string]interface{} `json:"ext"`
}

// HandleCallback handles callbacks from authentication server for OIDC flow
func (h *Handler) HandleCallback(req *restful.Request, res *restful.Response) {
	code := req.QueryParameter("code")
	idToken := req.QueryParameter("id_token")
	state := req.QueryParameter("state")
	logg := logger.With(log.String("param_code", code), log.String("param_id_token", idToken), log.String("param_state", state))
	var (
		err  error
		info RenewPayload
	)
	switch {
	case code != "":
		if state == "" || h.State != state {
			logg.Error("state is invalid", log.String("local_state", h.State))
			err = errors.NewBadRequest("state \"" + state + "\" is not valid")
			break
		}
		var config *Config
		config, err = h.genOIDCClient(req.Request)
		if err != nil {
			logg.Error("error fetching OIDC config", log.Err(err))
			break
		}
		logg.Debug("OIDC config generated successfully")
		ctx := gooidc.ClientContext(req.Request.Context(), config.HTTPClient)
		var token *oauth2.Token

		token, err = config.Config.Exchange(ctx, code)
		if err != nil {
			logg.Error("OIDC exchange error", log.Err(err))
			break
		}
		logg.Debug("OIDC exchange successful", log.Any("token", token))

		rawToken, ok := token.Extra("id_token").(string)
		if !ok {
			logg.Error("no id_token in token response", log.String("tokenType", token.TokenType))
			err = errors.NewBadRequest("no id_token in token response")
			break
		}
		logg.Debug("Got ID Token", log.String("id_token", rawToken))

		var idToken *gooidc.IDToken
		idToken, err = config.Verifier.Verify(ctx, rawToken)
		if err != nil {
			logg.Error("id_token is invalid", log.String("id_token", rawToken))
			err = errors.NewUnauthorized(err.Error())
			break
		}
		logg.Debug("ID Token verified successfully", log.Any("oidc_token", idToken))

		if idToken != nil && idToken.Nonce != h.Nonce {
			logg.Error("nonce is invalid", log.String("nonce", h.Nonce))
			err = errors.NewBadRequest("nonce \"" + h.Nonce + "\" is invalid")
			break
		}

		info = RenewPayload{
			AccessToken:  token.AccessToken,
			TokenType:    token.TokenType,
			RefreshToken: token.RefreshToken,
			IDToken:      rawToken,
			ExpireAt:     &idToken.Expiry,
			IssuedAt:     &idToken.IssuedAt,
		}

	case idToken != "":
		info = RenewPayload{
			IDToken: idToken,
		}

	default:
		err = errors.NewBadRequest("code or id_token must be provided")
	}
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(info)
}

// HandleInfo get user info from token
// Expectes users to provide a Bearer token on the Authorization header
// and will fetch and return information from it.
// will return 401 Unauthorized errors
func (h *Handler) HandleInfo(req *restful.Request, res *restful.Response) {
	var (
		err     error
		token   string
		config  *Config
		idToken *gooidc.IDToken
	)
	logg := logger.With(log.String("authorization", req.HeaderParameter("Authorization")))
	defer func() {
		if err != nil {
			// err = errors.NewUnauthorized(err.Error())
			h.Server.HandleError(err, req, res)
		}
	}()

	token, err = h.getToken(req)
	logg.Debug("Fetching token from Authorization header", log.String("token", token), log.Err(err))
	if err != nil {
		logg.Error("not possible get token from header", log.Err(err))
		return
	}
	logg = logg.With(log.String("token", token))

	// generates OIDC configuration and verifies the token
	// to make sure this is a valid ID Token
	config, err = h.genOIDCClient(req.Request)
	if err != nil {
		logg.Error("OIDC configuration error", log.Err(err))
		return
	}

	ctx := gooidc.ClientContext(req.Request.Context(), config.HTTPClient)
	idToken, err = config.Verifier.Verify(ctx, token)
	if err != nil {
		logg.Error("ID token is invalid", log.Err(err))
		return
	}

	// uses the id token to render user info
	userInfo := UserInfo{}
	if err = idToken.Claims(&userInfo); err != nil {
		logg.Error("Not possible to render claims", log.Err(err))
		return
	}

	res.WriteAsJson(userInfo)
}

// RenewPayload payload for refresh token request
type RenewPayload struct {
	TokenType    string     `json:"token_type,omitempty"`
	AccessToken  string     `json:"access_token,omitempty"`
	IDToken      string     `json:"id_token"`
	RefreshToken string     `json:"refresh_token"`
	ExpireAt     *time.Time `json:"expire_at,omitempty"`
	IssuedAt     *time.Time `json:"issued_at,omitempty"`
}

// HandleRefresh refresh tokens
func (h *Handler) HandleRefresh(req *restful.Request, res *restful.Response) {
	var (
		err     error
		token   string
		config  *Config
		idToken *gooidc.IDToken
	)
	defer func() {
		// handle error
		if err != nil {
			h.Server.HandleError(err, req, res)
			// res.WriteError(http.StatusUnauthorized, err)
		}
	}()
	token = strings.TrimSpace(req.QueryParameter("refresh_token"))
	logg := logger.With(log.String("refresh_token", token))
	if token == "" {
		log.Error("refresh_token is required")
		err = errors.NewBadRequest("refresh_token is required")
		return
	}

	if config, err = h.genOIDCClient(req.Request); err != nil {
		logg.Error("error generating OIDC config", log.Err(err))
		return
	}

	oauthToken := &oauth2.Token{
		RefreshToken: token,
		Expiry:       time.Now().Add(-time.Hour),
	}
	ctx := gooidc.ClientContext(req.Request.Context(), config.HTTPClient)
	if oauthToken, err = config.Config.TokenSource(ctx, oauthToken).Token(); err != nil {
		logg.Error("unable to refresh token", log.Err(err))
		return
	}

	rawToken, ok := oauthToken.Extra("id_token").(string)
	if !ok {
		logg.Error("no id_token in token response", log.String("tokenType", oauthToken.TokenType))
		err = errors.NewBadRequest("no id_token in token response")
		return
	}
	if idToken, err = config.Verifier.Verify(ctx, rawToken); err != nil {
		logg.Error("id_token is invalid", log.String("id_token", rawToken))
		return
	}
	if idToken != nil && idToken.Nonce != h.Nonce {
		logg.Error("nonce is invalid", log.String("nonce", h.Nonce))
		err = errors.NewBadRequest("nonce \"" + h.Nonce + "\" is invalid")
		return
	}

	info := RenewPayload{
		AccessToken:  oauthToken.AccessToken,
		TokenType:    oauthToken.TokenType,
		RefreshToken: oauthToken.RefreshToken,
		IDToken:      rawToken,
		ExpireAt:     &idToken.Expiry,
		IssuedAt:     &idToken.IssuedAt,
	}
	res.WriteAsJson(info)
}

// getToken returns the token from a request if any
func (h *Handler) getToken(req *restful.Request) (token string, err error) {
	// fetches token from header
	token = strings.TrimSpace(strings.TrimPrefix(req.HeaderParameter("Authorization"), "Bearer "))
	if token == "" {
		err = errors.NewUnauthorized("no Bearer token present on the Authorization header")
	}
	return
}

// genOIDCClient generates the client for this backend
func (h *Handler) genOIDCClient(req *http.Request) (config *Config, err error) {
	// TODO: find another way to make cache of this
	if h.config != nil && !h.checkExpire() {
		// return config
		config = h.config
		newConfig := h.configOverride(req, config)
		return newConfig, nil
	}
	// locking to generate oidc config
	config = &Config{}
	h.lock.Lock()
	defer func() {
		if err == nil && config.IsComplete() {
			h.config = config
			h.cacheExpireTime = time.Now().Add(h.CacheDuration * time.Second)
		}
		h.lock.Unlock()
	}()

	// generates ca certificates pool
	var cert *x509.CertPool
	cert, err = h.genCertPool()
	if err != nil {
		logger.Error("error generating root ca cert pool", log.Err(err))
		return
	}
	// generates http client
	config.HTTPClient = h.generateClient(cert)

	// generating provider
	ctx := gooidc.ClientContext(context.Background(), config.HTTPClient)
	config.Provider, err = gooidc.NewProvider(ctx, h.IssuerURL)
	if err != nil {
		logger.Error("error generating OIDC provider", log.Err(err))
		return
	}
	// config
	config.Config = &oauth2.Config{
		ClientID:     h.ClientID,
		ClientSecret: h.ClientSecret,
		Endpoint:     config.Provider.Endpoint(),
		RedirectURL:  h.RedirectURL,
		Scopes:       h.Scopes,
	}
	// options
	config.Options = []oauth2.AuthCodeOption{
		oauth2.AccessTypeOffline,
		oauth2.SetAuthURLParam("response_type", h.ResponseType),
		oauth2.SetAuthURLParam("nonce", h.Nonce),
	}
	config.Verifier = config.Provider.Verifier(&gooidc.Config{ClientID: h.ClientID})
	newConfig := h.configOverride(req, config)
	return newConfig, nil
}

func (h *Handler) genCertPool() (cert *x509.CertPool, err error) {
	if h.CAFile != "" {
		cert = x509.NewCertPool()
		var ca []byte
		ca, err = ioutil.ReadFile(h.CAFile)
		if err != nil {
			logger.Error("Failed to read the OIDC CA certificate file", log.String("file", h.CAFile), log.Err(err))
			return
		}
		cert.AppendCertsFromPEM(ca)
	}
	return
}

func (h *Handler) generateClient(cert *x509.CertPool) (client *http.Client) {
	var tlsConfig *tls.Config
	if cert == nil {
		tlsConfig = &tls.Config{
			InsecureSkipVerify: true,
		}
	} else {
		tlsConfig = &tls.Config{
			RootCAs: cert,
		}
	}

	if h.HTTPProxy != nil {
		logger.Debug("http proxy settings", log.Any("config", h.HTTPProxy))
		h.proxyLock.Do(func() {
			proxyFunc := h.HTTPProxy.ProxyFunc()
			h.httpProxyFunc = func(req *http.Request) (*url.URL, error) {
				logger.Debug("will proxy request", log.Any("req", req.URL))
				url, err := proxyFunc(req.URL)
				if url == nil {
					logger.Debug("url could not be proxied. If username and password are used in the proxy address make sure to use the right encoding")
				}
				return url, err
			}
		})
	}

	return &http.Client{
		Transport: &http.Transport{
			Proxy: h.httpProxyFunc,
			DialContext: (&net.Dialer{
				Timeout:   h.Timeout,
				KeepAlive: 30 * time.Second,
				DualStack: true,
			}).DialContext,
			MaxIdleConns:          100,
			IdleConnTimeout:       90 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
			TLSClientConfig:       tlsConfig,
		},
	}
}

func (h *Handler) checkExpire() bool {
	now := time.Now()
	if h.cacheExpireTime.Before(now) {
		return true
	}
	return false
}
