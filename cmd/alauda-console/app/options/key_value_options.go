package options

import (
	"fmt"
	"strings"
	"sync"

	"bitbucket.org/mathildetech/alauda-console/pkg/console"
)

// KeyValue key value struct
type KeyValue struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func (kv KeyValue) String() string {
	return fmt.Sprintf("%s=%s", kv.Key, kv.Value)
}

// Set sets a new val
func (kv *KeyValue) Set(val string) error {
	splitted := strings.SplitN(val, "=", 2)
	kv.Key = splitted[0]
	kv.Value = splitted[1]
	return nil
}

// Type for KeyValue
func (kv KeyValue) Type() string {
	return "KeyValue"
}

// NewKeyValue sets a new KeyValue flag
func NewKeyValue(val string) (kv *KeyValue, err error) {
	splitted := strings.SplitN(val, "=", 2)
	if len(splitted) < 1 {
		err = fmt.Errorf("Needs key and value: %s", val)
		return
	}
	kv = &KeyValue{}
	kv.Key = splitted[0]
	kv.Value = splitted[1]
	return
}

// KeyValueSlice slice of keyValues
type KeyValueSlice struct {
	items        []*KeyValue
	isSet        bool
	prestineLock sync.Once
}

// NewKeyValueSlice constructor for *KeyValueSlice
func NewKeyValueSlice() *KeyValueSlice {
	return &KeyValueSlice{items: []*KeyValue{}}
}

func (kv *KeyValueSlice) markAsSet() {
	kv.prestineLock.Do(func() {
		kv.isSet = true
	})
}

// Append adds the specified value to the end of the flag value list.
func (kv *KeyValueSlice) Append(val string) error {
	item, err := NewKeyValue(val)
	if err != nil {
		return err
	}
	kv.markAsSet()
	kv.items = append(kv.items, item)
	return nil
}

// Replace will fully overwrite any data currently in the flag value list.
func (kv *KeyValueSlice) Replace(vals []string) error {
	newKv := &KeyValueSlice{}
	if len(vals) > 0 {
		for _, val := range vals {
			if err := newKv.Append(val); err != nil {
				return err
			}
		}
	}
	*kv = *newKv
	kv.markAsSet()
	return nil
}

// GetSlice returns the flag value list as an array of strings.
func (kv KeyValueSlice) GetSlice() []string {
	vals := make([]string, 0, len(kv.items))
	if len(kv.items) > 0 {
		for _, k := range kv.items {
			vals = append(vals, k.String())
		}
	}
	return vals
}

// String returns a string for value
func (kv KeyValueSlice) String() string {
	value := strings.Join(kv.GetSlice(), ",")
	return value
}

// Set set string
func (kv *KeyValueSlice) Set(vals string) error {
	values := strings.Split(vals, ",")
	for _, val := range values {
		if err := kv.Append(val); err != nil {
			return err
		}
	}
	kv.markAsSet()
	return nil
}

// Type return a type for flag
func (kv KeyValueSlice) Type() string {
	return "KeyValueSlice"
}

// Export export items as []console.KeyValue
func (kv KeyValueSlice) Export() (envVars []console.KeyValue) {
	for _, item := range kv.items {
		envVars = append(envVars, console.KeyValue{Key: item.Key, Value: item.Value})
	}
	return
}

// IsSet returns true if any value was set
func (kv KeyValueSlice) IsSet() bool {
	return kv.isSet
}
