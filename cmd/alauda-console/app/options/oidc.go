package options

import (
	"fmt"
	"time"

	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/alauda-console/pkg/oidc"
	gooidc "github.com/coreos/go-oidc"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"golang.org/x/net/http/httpproxy"
)

const (
	flagOIDCClientSecret     = "oidc-client-secret"
	flagOIDCIssuerURL        = "oidc-issuer-url"
	flagOIDCClientID         = "oidc-client-id"
	flagOIDCCAFile           = "oidc-ca-file"
	flagOIDCUsernameClaim    = "oidc-username-claim"
	flagOIDCUsernamePrefix   = "oidc-username-prefix"
	flagOIDCGroupsPrefix     = "oidc-groups-prefix"
	flagOIDCGroupsClaim      = "oidc-groups-claim"
	flagOIDCTenantIDClaim    = "oidc-tenantid-claim"
	flagOIDCTenantIDPrefix   = "oidc-tenantid-prefix"
	flagOIDCSigningAlgs      = "oidc-signing-algs"
	flagOIDCRequiredClaims   = "oidc-required-claim"
	flagOIDCScopes           = "oidc-scopes"
	flagOIDCRedirectURL      = "oidc-redirect-url"
	flagOIDCTimeout          = "oidc-timeout"
	flagOIDCResponseType     = "oidc-response-type"
	flagOIDCState            = "oidc-state"
	flagOIDCNonce            = "oidc-nonce"
	flagOIDCProtocolOverride = "oidc-protocol-override"
	flagOIDCConfigCacheTime  = "oidc-config-cache-time"
	flagHTTPProxy            = "http-proxy"
	flagHTTPSProxy           = "https-proxy"
	flagNoProxy              = "no-proxy"
	flagCGIRequestMethod     = "cgi-request-method"
	flagUseEnvironmentProxy  = "use-environment-proxy"
)

const (
	configOIDCClientSecret     = "authentication.oidc_client_secret"
	configOIDCIssuerURL        = "authentication.oidc_issuer_url"
	configOIDCClientID         = "authentication.oidc_client_id"
	configOIDCCAFile           = "authentication.oidc_ca_file"
	configOIDCUsernameClaim    = "authentication.oidc_username_claim"
	configOIDCUsernamePrefix   = "authentication.oidc_username_prefix"
	configOIDCGroupsPrefix     = "authentication.oidc_groups_prefix"
	configOIDCGroupsClaim      = "authentication.oidc_groups_claim"
	configOIDCTenantIDClaim    = "authentication.oidc_tenantid_claim"
	configOIDCTenantIDPrefix   = "authentication.oidc_tenantid_prefix"
	configOIDCSigningAlgs      = "authentication.oidc_signing_algs"
	configOIDCRequiredClaims   = "authentication.oidc_required_claim"
	configOIDCScopes           = "authentication.oidc_scopes"
	configOIDCRedirectURL      = "authentication.oidc_redirect_url"
	configOIDCTimeout          = "authentication.oidc_timeout"
	configOIDCResponseType     = "authentication.oidc_response_type"
	configOIDCNonce            = "authentication.nonce"
	configOIDCState            = "authentication.state"
	configOIDCProtocolOverride = "authentication.oidc_protocol_override"
	configOIDCCacheTime        = "authentication.oidc_config_cache_time"
	configHTTPProxy            = "authentication.http_proxy"
	configHTTPSProxy           = "authentication.https_proxy"
	configNoProxy              = "authentication.no_proxy"
	configCGIRequestMethod     = "authentication.cgi_request_method"
	configUseEnvironmentProxy  = "authentication.use_environment_proxy"
)

// OIDCOptions defines the configuration options needed to initialize
// OpenID Connect authentication.
type OIDCOptions struct {
	// root ca certificate file path
	CAFile string
	// OIDC client id
	ClientID string
	// OIDC issuer url
	IssuerURL string
	// username claim for OIDC. defaults to email
	UsernameClaim string
	// if any other claim is used for OIDC other than email, a prefix must be given,
	// e.g alauda:
	UsernamePrefix string
	// Groups claim for OIDC, defaults to groups
	GroupsClaim string

	GroupsPrefix   string
	TenantIDClaim  string
	TenantIDPrefix string
	// Signing algorithms accepted by OIDC, defaults to RS256
	SigningAlgs    []string
	RequiredClaims map[string]string
	// Client secret used on OIDC authentication flows
	ClientSecret string
	// RedirectURL used when login in
	RedirectURL string
	// Timeout duration for client negotiations with OIDC provider. defaults to 5s
	Timeout time.Duration
	// Required scopes for OIDC provider, defaults to [openid, profile, offline_access, email, groups, ext]
	Scopes []string
	// Defines the OIDC authorization flow, defaults to code for implicit flow
	ResponseType string
	// State unique static string used to prevent man in the middle attacks with OIDC
	// used to validate callbacks from OIDC
	State string
	// Nonce unique static string used to prevent man in the middle attacks with OIDC.
	// Used to validate OIDC ID tokens
	Nonce string

	// ProtocolOverride used to override the protocol provided
	// by OIDC issuer when returning a login url
	ProtocolOverride string
	// oidc config cache seconds
	CacheDuration time.Duration

	// HTTP Proxy options
	HTTPProxy           string
	HTTPSProxy          string
	NoProxy             string
	CGIRequestMethod    string
	UseEnvironmentProxy bool

	httpProxyConfig *httpproxy.Config

	// Whether to disable dynamic url
	DisableDynamicURL bool
}

// NewOIDCOptions creates the default OIDCOptions object.
func NewOIDCOptions() *OIDCOptions {
	return &OIDCOptions{
		UsernameClaim: "email",
		ResponseType:  "code",
		SigningAlgs:   []string{"RS256"},
		Timeout:       time.Second * 5,
		Scopes:        []string{gooidc.ScopeOpenID, "profile", "offline_access", "email", "groups", "ext"},
		State:         "alauda-console",
		Nonce:         "alauda-console",
	}
}

// AddFlags adds flags for log to the specified FlagSet object.
func (o *OIDCOptions) AddFlags(fs *pflag.FlagSet) {
	fs.String(flagOIDCIssuerURL, o.IssuerURL, ""+
		"The URL of the OpenID issuer, only HTTPS scheme will be accepted. "+
		"If set, it will be used to verify the OIDC JSON Web Token (JWT).")
	_ = viper.BindPFlag(configOIDCIssuerURL, fs.Lookup(flagOIDCIssuerURL))
	fs.String(flagOIDCClientID, o.ClientID,
		"The client ID for the OpenID Connect client, must be set if oidc-issuer-url is set.")
	_ = viper.BindPFlag(configOIDCClientID, fs.Lookup(flagOIDCClientID))
	fs.String(flagOIDCCAFile, o.CAFile, ""+
		"If set, the OpenID server's certificate will be verified by one of the authorities "+
		"in the oidc-ca-file, otherwise the host's root CA set will be used.")
	_ = viper.BindPFlag(configOIDCCAFile, fs.Lookup(flagOIDCCAFile))
	fs.String(flagOIDCUsernameClaim, o.UsernameClaim, ""+
		"The OpenID claim to use as the user name. Note that claims other than the default ('sub') "+
		"is not guaranteed to be unique and immutable.")
	_ = viper.BindPFlag(configOIDCUsernameClaim, fs.Lookup(flagOIDCUsernameClaim))
	fs.String(flagOIDCUsernamePrefix, o.UsernamePrefix, ""+
		"If provided, all usernames will be prefixed with this value. If not provided, "+
		"username claims other than 'email' are prefixed by the issuer URL to avoid "+
		"clashes. To skip any prefixing, provide the value '-'.")
	_ = viper.BindPFlag(configOIDCUsernamePrefix, fs.Lookup(flagOIDCUsernamePrefix))
	fs.String(flagOIDCTenantIDClaim, o.TenantIDClaim,
		"If provided, the name of a custom OpenID Connect claim for specifying user tenant id.")
	_ = viper.BindPFlag(configOIDCTenantIDClaim, fs.Lookup(flagOIDCTenantIDClaim))
	fs.String(flagOIDCTenantIDPrefix, o.TenantIDPrefix,
		"If provided, all tenant ids will be prefixed with this value to prevent conflicts with "+
			"other authentication strategies.")
	_ = viper.BindPFlag(configOIDCTenantIDPrefix, fs.Lookup(flagOIDCTenantIDPrefix))
	fs.String(flagOIDCGroupsClaim, o.GroupsClaim, ""+
		"If provided, the name of a custom OpenID Connect claim for specifying user groups. "+
		"The claim value is expected to be a string or array of strings. ")
	_ = viper.BindPFlag(configOIDCGroupsClaim, fs.Lookup(flagOIDCGroupsClaim))
	fs.String(flagOIDCGroupsPrefix, o.GroupsPrefix, ""+
		"If provided, all groups will be prefixed with this value to prevent conflicts with "+
		"other authentication strategies.")
	_ = viper.BindPFlag(configOIDCGroupsPrefix, fs.Lookup(flagOIDCGroupsPrefix))
	fs.StringSlice(flagOIDCSigningAlgs, o.SigningAlgs, ""+
		"Comma-separated list of allowed JOSE asymmetric signing algorithms. JWTs with a "+
		"'alg' header value not in this list will be rejected. "+
		"Values are defined by RFC 7518 https://tools.ietf.org/html/rfc7518#section-3.1.")
	_ = viper.BindPFlag(configOIDCSigningAlgs, fs.Lookup(flagOIDCSigningAlgs))
	fs.String(flagOIDCRequiredClaims, "", ""+
		"A key=value pair that describes a required claim in the ID Token. "+
		"If set, the claim is verified to be present in the ID Token with a matching value. "+
		"Repeat this flag to specify multiple claims.")
	_ = viper.BindPFlag(configOIDCRequiredClaims, fs.Lookup(flagOIDCRequiredClaims))
	fs.String(flagOIDCClientSecret, o.ClientSecret,
		"The client secret for the OpenID connect client, must be set if oidc-issuer-url is set.")
	_ = viper.BindPFlag(configOIDCClientSecret, fs.Lookup(flagOIDCClientSecret))

	fs.String(flagOIDCRedirectURL, o.RedirectURL,
		"The redirect url the OpenID connect client, must be set if oidc-issuer-url is set.")
	_ = viper.BindPFlag(configOIDCRedirectURL, fs.Lookup(flagOIDCRedirectURL))

	fs.StringSlice(flagOIDCScopes, o.Scopes,
		"Scopes requested for OIDC ID Token, must be set if oidc-issuer-url is set.")
	_ = viper.BindPFlag(configOIDCScopes, fs.Lookup(flagOIDCScopes))

	fs.Duration(flagOIDCTimeout, o.Timeout,
		"The timeout when requesting the issuer for the OpenID connect client, must be set if oidc-issuer-url is set.")
	_ = viper.BindPFlag(configOIDCTimeout, fs.Lookup(flagOIDCTimeout))

	fs.String(flagOIDCResponseType, o.ResponseType,
		"Response type for OIDC authentication flow, must be set if oidc-issuer-url is set.")
	_ = viper.BindPFlag(configOIDCResponseType, fs.Lookup(flagOIDCResponseType))

	fs.String(flagOIDCState, o.State,
		"Unique static string used to prevent man in the middle attacks with OIDC, must be set if oidc-issuer-url is set.")
	_ = viper.BindPFlag(configOIDCState, fs.Lookup(flagOIDCState))

	fs.String(flagOIDCNonce, o.Nonce,
		"Unique static string used to prevent man in the middle attacks with OIDC, must be set if oidc-issuer-url is set.")
	_ = viper.BindPFlag(configOIDCNonce, fs.Lookup(flagOIDCNonce))

	fs.String(flagOIDCProtocolOverride, o.ProtocolOverride,
		"Protocol override for OIDC issuer authentication URL. will replace any given protocol (http or https) to the desired protocol.")
	_ = viper.BindPFlag(configOIDCProtocolOverride, fs.Lookup(flagOIDCProtocolOverride))

	fs.Duration(flagOIDCConfigCacheTime, o.CacheDuration, "OIDC Config cache time.")
	_ = viper.BindPFlag(configOIDCCacheTime, fs.Lookup(flagOIDCConfigCacheTime))
	viper.SetDefault(configOIDCCacheTime, 120)

	// HTTP Proxy
	fs.String(flagHTTPProxy, o.HTTPProxy,
		"HTTP Proxy address. Needs to add username and password to address if needed.")
	_ = viper.BindPFlag(configHTTPProxy, fs.Lookup(flagHTTPProxy))
	fs.String(flagHTTPSProxy, o.HTTPSProxy,
		"HTTPS Proxy address. Needs to add username and password to address if needed.")
	_ = viper.BindPFlag(configHTTPSProxy, fs.Lookup(flagHTTPSProxy))
	fs.String(flagNoProxy, o.NoProxy,
		"No Proxy address. Adds any domains or address that does not need proxy.")
	_ = viper.BindPFlag(configNoProxy, fs.Lookup(flagNoProxy))
	fs.String(flagCGIRequestMethod, o.CGIRequestMethod,
		"CGI request method. see golang.org/s/cgihttpproxy")
	_ = viper.BindPFlag(configCGIRequestMethod, fs.Lookup(flagCGIRequestMethod))
	fs.Bool(flagUseEnvironmentProxy, o.UseEnvironmentProxy,
		"Use environment variables as proxy. If enabled will override other proxy related configuration")
	_ = viper.BindPFlag(configUseEnvironmentProxy, fs.Lookup(flagUseEnvironmentProxy))
}

// ApplyFlags parsing parameters from the command line or configuration file
// to the options instance.
func (o *OIDCOptions) ApplyFlags() []error {
	var errs []error

	o.CAFile = viper.GetString(configOIDCCAFile)
	o.ClientID = viper.GetString(configOIDCClientID)
	o.GroupsClaim = viper.GetString(configOIDCGroupsClaim)
	o.GroupsPrefix = viper.GetString(configOIDCGroupsPrefix)
	o.TenantIDPrefix = viper.GetString(configOIDCTenantIDPrefix)
	o.TenantIDClaim = viper.GetString(configOIDCTenantIDClaim)
	o.IssuerURL = viper.GetString(configOIDCIssuerURL)
	o.RequiredClaims = viper.GetStringMapString(configOIDCRequiredClaims)
	o.SigningAlgs = viper.GetStringSlice(configOIDCSigningAlgs)
	o.UsernameClaim = viper.GetString(configOIDCUsernameClaim)
	o.UsernamePrefix = viper.GetString(configOIDCUsernamePrefix)
	o.ClientSecret = viper.GetString(configOIDCClientSecret)
	o.RedirectURL = viper.GetString(configOIDCRedirectURL)
	o.Scopes = viper.GetStringSlice(configOIDCScopes)
	o.Timeout = viper.GetDuration(configOIDCTimeout)
	o.ResponseType = viper.GetString(configOIDCResponseType)
	o.Nonce = viper.GetString(configOIDCNonce)
	o.State = viper.GetString(configOIDCState)
	o.ProtocolOverride = viper.GetString(configOIDCProtocolOverride)
	o.CacheDuration = viper.GetDuration(configOIDCCacheTime)
	o.HTTPProxy = viper.GetString(configHTTPProxy)
	o.HTTPSProxy = viper.GetString(configHTTPSProxy)
	o.NoProxy = viper.GetString(configNoProxy)
	o.CGIRequestMethod = viper.GetString(configCGIRequestMethod)
	o.UseEnvironmentProxy = viper.GetBool(configUseEnvironmentProxy)
	o.DisableDynamicURL = viper.GetBool(configDisableDynamicURL)

	if o.IssuerURL != "" {
		if o.ClientSecret == "" {
			errs = append(errs, fmt.Errorf(flagOIDCClientSecret+" must be specified"))
		}
		if o.RedirectURL == "" {
			errs = append(errs, fmt.Errorf(flagOIDCRedirectURL+" must be specified"))
		}
		if o.State == "" {
			errs = append(errs, fmt.Errorf(flagOIDCState+" must be specified"))
		}
		if o.Nonce == "" {
			errs = append(errs, fmt.Errorf(flagOIDCNonce+" must be specified"))
		}
		if len(o.Scopes) == 0 {
			errs = append(errs, fmt.Errorf(flagOIDCScopes+" must be specified"))
		}
	}

	if o.UseEnvironmentProxy {
		o.httpProxyConfig = httpproxy.FromEnvironment()
	} else if o.HTTPProxy != "" || o.HTTPSProxy != "" {
		o.httpProxyConfig = &httpproxy.Config{
			HTTPProxy:  o.HTTPProxy,
			HTTPSProxy: o.HTTPSProxy,
			NoProxy:    o.NoProxy,
			CGI:        o.CGIRequestMethod != "",
		}
	}

	return errs
}

// ApplyToServer apply options on server
func (o *OIDCOptions) ApplyToServer(srv server.Server) error {
	handler := oidc.NewHandler()
	handler.CAFile = o.CAFile
	handler.ClientID = o.ClientID
	handler.GroupsClaim = o.GroupsClaim
	handler.GroupsPrefix = o.GroupsPrefix
	handler.TenantIDPrefix = o.TenantIDPrefix
	handler.TenantIDClaim = o.TenantIDClaim
	handler.IssuerURL = o.IssuerURL
	handler.RequiredClaims = o.RequiredClaims
	handler.SigningAlgs = o.SigningAlgs
	handler.UsernameClaim = o.UsernameClaim
	handler.UsernamePrefix = o.UsernamePrefix
	handler.ClientSecret = o.ClientSecret
	handler.RedirectURL = o.RedirectURL
	handler.Scopes = o.Scopes
	handler.Timeout = o.Timeout
	handler.ResponseType = o.ResponseType
	handler.Nonce = o.Nonce
	handler.State = o.State
	handler.ProtocolOverride = o.ProtocolOverride
	handler.CacheDuration = o.CacheDuration
	handler.HTTPProxy = o.httpProxyConfig
	handler.DisableDynamicURL = o.DisableDynamicURL
	return handler.ApplyToServer(srv)
}
