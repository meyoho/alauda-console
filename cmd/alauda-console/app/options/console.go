package options

import (
	"fmt"

	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/alauda-console/pkg/console"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	flagAPIAddress           = "api-address"
	flagConsoleAssetsPath    = "assets-host-path"
	flagConsoleAssetsURLPath = "assets-url-path"
	flagConsoleEnvironment   = "envs"
)

const (
	configAPIAddress           = "console.api_address"
	configConsoleAssetsPath    = "console.assets_host_path"
	configConsoleAssetsURLPath = "console.assets_url_path"
	configConsoleCustomEnvVars = "console.envs"
)

// ConsoleOptions contains configuration items related to console attributes.
type ConsoleOptions struct {
	*console.Console
	*KeyValueSlice
}

// NewConsoleOptions creates a ConsoleOptions object with default parameters.
func NewConsoleOptions() *ConsoleOptions {
	cs := console.New()
	cs.APIAddress = "http://127.0.0.1:8080"
	cs.AssetsHostPath = "."
	cs.AssetsURLPath = "/"
	return &ConsoleOptions{
		Console:       cs,
		KeyValueSlice: NewKeyValueSlice(),
	}
}

// AddFlags adds flags for console to the specified FlagSet object.
func (o *ConsoleOptions) AddFlags(fs *pflag.FlagSet) {
	fs.String(flagAPIAddress, o.APIAddress,
		"The address of the api gateway, including the scheme, host, and port.")
	_ = viper.BindPFlag(configAPIAddress, fs.Lookup(flagAPIAddress))

	fs.String(flagConsoleAssetsPath, o.AssetsHostPath,
		"Path to the front-end file assets on the server.")
	_ = viper.BindPFlag(configConsoleAssetsPath, fs.Lookup(flagConsoleAssetsPath))

	fs.String(flagConsoleAssetsURLPath, o.AssetsURLPath,
		"Path to the front-end file assets on url.")
	_ = viper.BindPFlag(configConsoleAssetsURLPath, fs.Lookup(flagConsoleAssetsURLPath))

	fs.Var(o.KeyValueSlice, flagConsoleEnvironment, "Custom environment variables to return on env api.")
	_ = viper.BindPFlag(configConsoleCustomEnvVars, fs.Lookup(flagConsoleEnvironment))
}

// ApplyFlags parsing parameters from the command line or configuration file
// to the options instance.
func (o *ConsoleOptions) ApplyFlags() []error {
	var errs []error

	o.AssetsHostPath = viper.GetString(configConsoleAssetsPath)
	o.APIAddress = viper.GetString(configAPIAddress)
	o.AssetsURLPath = viper.GetString(configConsoleAssetsURLPath)
	o.CustomEnvVars = o.KeyValueSlice.Export()
	o.DisableDynamicURL = viper.GetBool(configDisableDynamicURL)

	// if set via commandline arguments it will be not empty, so we should skip this
	// if empty or not set it means it came through config file and it was not properly set
	// because it is not a string, therefore we need to unmarshal it seperatly
	// this means that they are mutual exclusive
	if !o.IsSet() || len(o.CustomEnvVars) == 0 {
		if err := viper.UnmarshalKey(configConsoleCustomEnvVars, &o.CustomEnvVars); err != nil {
			errs = append(errs, fmt.Errorf("%s does not contain a slice of key,value structure. The expected format is [{\"key\": \"\", \"value\":\"\"}]. Decoding error: %s", configConsoleCustomEnvVars, err))
		}
	}

	if o.APIAddress == "" {
		errs = append(errs, fmt.Errorf("--%s must be specified", flagAPIAddress))
	}
	if o.AssetsHostPath == "" {
		errs = append(errs, fmt.Errorf("assets-host-path must be specified"))
	}
	if o.AssetsURLPath == "" {
		errs = append(errs, fmt.Errorf("assets-url-path must be specified"))
	}

	return errs
}

// ApplyToServer apply options on server
func (o *ConsoleOptions) ApplyToServer(srv server.Server) error {
	return o.Console.ApplyToServer(srv)
}
