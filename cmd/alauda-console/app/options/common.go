package options

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	flagDisableDynamicURL = "disable-dynamic-url"
)

const (
	configDisableDynamicURL = "common.disable_dynamic_url"
)

// CommonOptions is an empty struct to implement Optioner interface
type CommonOptions struct{}

// NewCommonOptions creates a CommonOptions object with default parameters.
func NewCommonOptions() *CommonOptions {
	return &CommonOptions{}
}

// AddFlags adds flags for console to the specified FlagSet object.
func (o *CommonOptions) AddFlags(fs *pflag.FlagSet) {
	fs.Bool(flagDisableDynamicURL, false,
		"Whether to disable the behavior of dynamically returning url")
	_ = viper.BindPFlag(configDisableDynamicURL, fs.Lookup(flagDisableDynamicURL))
}

// ApplyFlags parsing parameters from the command line or configuration file
// to the options instance.
func (o *CommonOptions) ApplyFlags() []error {
	return []error{}
}

// ApplyToServer apply options on server
func (o *CommonOptions) ApplyToServer(srv server.Server) error {
	return nil
}
