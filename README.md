# alauda-console

Backend service for all UI projects

## Functions

1. OIDC client
2. Static file provider
3. EnvVars and Settings API

## Development

Check the [CONTRIBUTING.md](CONTRIBUTING.md) guide

## Usage

Console contains multiple flags for specific functions, below is an overview fetched using `--help` 

```
$ console --help

The application is a WEB server. In addition to providing services for front-end
static files, it also provides a query interface for Alauda global configuration.

Usage:
  alauda-console [flags]

Flags:
  -C, --config FILE                       Read configuration from specified FILE, support JSON, TOML, YAML, HCL, or Java properties formats.
      --log-level LEVEL                   Minimum log output LEVEL. (default "info")
      --log-format FORMAT                 Log output FORMAT, support plain or json format. (default "console")
      --log-disable-color                 Disable output ansi colors in plain format logs.
      --log-enable-caller                 Enable output of caller information in the log.
      --log-output-paths strings          Output paths of log (default [stdout])
      --log-error-output-paths strings    Error output paths of log (default [stderr])
      --request-log                       Enable request logs as debug (default true)
      --insecure-bind-address ip          The IP address on which to serve the --insecure-port (set to 0.0.0.0 for all IPv4 interfaces and :: for all IPv6 interfaces). (default 0.0.0.0)
      --insecure-port int                 The port on which to serve unsecured, unauthenticated access. (default 8080)
      --health-check                      Enables health check endpoint /healthz on server. (default true)
      --path-prefix string                Sets a path prefix as instruction for other services
      --metrics                           Enable metrics for prometheus web interface host:port/metrics (default true)
      --disable-dynamic-url               Whether to disable the behavior of dynamically returning url
      --oidc-issuer-url string            The URL of the OpenID issuer, only HTTPS scheme will be accepted. If set, it will be used to verify the OIDC JSON Web Token (JWT).
      --oidc-client-id string             The client ID for the OpenID Connect client, must be set if oidc-issuer-url is set.
      --oidc-ca-file string               If set, the OpenID server's certificate will be verified by one of the authorities in the oidc-ca-file, otherwise the host's root CA set will be used.
      --oidc-username-claim string        The OpenID claim to use as the user name. Note that claims other than the default ('sub') is not guaranteed to be unique and immutable. (default "email")
      --oidc-username-prefix string       If provided, all usernames will be prefixed with this value. If not provided, username claims other than 'email' are prefixed by the issuer URL to avoid clashes. To skip any prefixing, provide the value '-'.
      --oidc-tenantid-claim string        If provided, the name of a custom OpenID Connect claim for specifying user tenant id.
      --oidc-tenantid-prefix string       If provided, all tenant ids will be prefixed with this value to prevent conflicts with other authentication strategies.
      --oidc-groups-claim string          If provided, the name of a custom OpenID Connect claim for specifying user groups. The claim value is expected to be a string or array of strings. 
      --oidc-groups-prefix string         If provided, all groups will be prefixed with this value to prevent conflicts with other authentication strategies.
      --oidc-signing-algs strings         Comma-separated list of allowed JOSE asymmetric signing algorithms. JWTs with a 'alg' header value not in this list will be rejected. Values are defined by RFC 7518 https://tools.ietf.org/html/rfc7518#section-3.1. (default [RS256])
      --oidc-required-claim string        A key=value pair that describes a required claim in the ID Token. If set, the claim is verified to be present in the ID Token with a matching value. Repeat this flag to specify multiple claims.
      --oidc-client-secret string         The client secret for the OpenID connect client, must be set if oidc-issuer-url is set.
      --oidc-redirect-url string          The redirect url the OpenID connect client, must be set if oidc-issuer-url is set.
      --oidc-scopes strings               Scopes requested for OIDC ID Token, must be set if oidc-issuer-url is set. (default [openid,profile,offline_access,email,groups,ext])
      --oidc-timeout duration             The timeout when requesting the issuer for the OpenID connect client, must be set if oidc-issuer-url is set. (default 5s)
      --oidc-response-type string         Response type for OIDC authentication flow, must be set if oidc-issuer-url is set. (default "code")
      --oidc-state string                 Unique static string used to prevent man in the middle attacks with OIDC, must be set if oidc-issuer-url is set. (default "alauda-console")
      --oidc-nonce string                 Unique static string used to prevent man in the middle attacks with OIDC, must be set if oidc-issuer-url is set. (default "alauda-console")
      --oidc-protocol-override string     Protocol override for OIDC issuer authentication URL. will replace any given protocol (http or https) to the desired protocol.
      --oidc-config-cache-time duration   OIDC Config cache time.
      --http-proxy string                 HTTP Proxy address. Needs to add username and password to address if needed.
      --https-proxy string                HTTPS Proxy address. Needs to add username and password to address if needed.
      --no-proxy string                   No Proxy address. Adds any domains or address that does not need proxy.
      --cgi-request-method string         CGI request method. see golang.org/s/cgihttpproxy
      --use-environment-proxy             Use environment variables as proxy. If enabled will override other proxy related configuration
      --api-address string                The address of the api gateway, including the scheme, host, and port. (default "http://127.0.0.1:8080")
      --assets-host-path string           Path to the front-end file assets on the server. (default ".")
      --assets-url-path string            Path to the front-end file assets on url. (default "/")
      --envs KeyValueSlice                Custom environment variables to return on env api.
      --swagger                           Enable swagger api docs on /swagger.json (default true)
      --swagger-ui                        Enable swagger ui on /swagger-ui if swagger api docs is enabled (default true)
  -V, --version version[=true]            Print version information and quit.
  -H, --help                              Help for Alauda Console.
```

## Logging

Most of log functionality behaviour is controlled by flags:

| flag          | values         | Description |
| ------------- | -------------- | ----------- |
| `--log-level` | `debug` `info` `warn` `error` `dpanic` `panic` `fatal` |             |

