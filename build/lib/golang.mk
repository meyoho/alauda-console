# Copyright 2019 THL A29 Limited, a Tencent company.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# ==============================================================================
# Makefile helper functions for golang
#

GO := go
GO_SUPPORTED_VERSIONS ?= 1.11|1.12|1.13
GO_LDFLAGS += -X $(VERSION_PACKAGE).GitVersion=$(VERSION) -X $(VERSION_PACKAGE).BuildDate=$(shell date -u +'%Y-%m-%dT%H:%M:%SZ')
GO_PROXY = ${GOPROXY}
GO_MODULES = ${GO111MODULE}

ifeq ($(GOOS),windows)
GO_OUT_EXT := .exe
endif

ifeq ($(GOOS),windows)
GO_PROVIDER_EXT := .dll
else
GO_PROVIDER_EXT := .so
endif

ifeq ($(ROOT_PACKAGE),)
$(error the variable ROOT_PACKAGE must be set prior to including golang.mk)
endif

ifeq ($(origin PLATFORM), undefined)
ifeq ($(origin GOOS), undefined)
GOOS := $(shell go env GOOS)
endif
ifeq ($(origin GOARCH), undefined)
GOARCH := $(shell go env GOARCH)
endif
PLATFORM := $(GOOS)_$(GOARCH)
else
GOOS := $(word 1, $(subst _, ,$

GOARCH := $(word 2, $(subst _, ,$(PLATFORM)))
export GOOS
export GOARCH
endif

GOHOSTOS := $(shell go env GOHOSTOS)
GOHOSTARCH := $(shell go env GOHOSTARCH)
HOST_PLATFORM := $(GOHOSTOS)_$(GOHOSTARCH)

PLATFORMS ?= darwin_amd64 windows_amd64 linux_amd64 linux_arm64
COMMANDS=$(wildcard ${ROOT_DIR}/cmd/*)
BINS=$(foreach cmd,${COMMANDS},$(notdir ${cmd}))

ifeq (${COMMANDS},)
  $(error Could not determine COMMANDS, set ROOT_DIR or run in source dir)
endif
ifeq (${BINS},)
  $(error Could not determine BINS, set ROOT_DIR or run in source dir)
endif

.PHONY: go.build.verify
go.build.verify:
ifneq ($(shell $(GO) version | grep -q -E '\bgo($(GO_SUPPORTED_VERSIONS))\b' && echo 0 || echo 1), 0)
	$(error unsupported go version. Please make install one of the following supported version: '$(GO_SUPPORTED_VERSIONS)')
endif

.PHONY: go.build.%
go.build.%:
	$(eval COMMAND := $(word 2,$(subst ., ,$*)))
	$(eval PLATFORM := $(word 1,$(subst ., ,$*)))
	$(eval OS := $(word 1,$(subst _, ,$(PLATFORM))))
	$(eval ARCH := $(word 2,$(subst _, ,$(PLATFORM))))
	@echo "===========> Building binary $(COMMAND) $(VERSION) for $(OS) $(ARCH)"
	@mkdir -p $(OUTPUT_DIR)/$(OS)/$(ARCH)
	@CGO_ENABLED=0 GOPROXY=${GO_PROXY} GO111MODULE=${GO_MODULES} GOOS=$(OS) GOARCH=$(ARCH) $(GO) build -v -o $(OUTPUT_DIR)/$(OS)/$(ARCH)/$(COMMAND)$(GO_OUT_EXT) -ldflags "$(GO_LDFLAGS)" $(ROOT_PACKAGE)/cmd/$(COMMAND)

.PHONY: go.build
go.build: go.build.verify $(addprefix go.build., $(addprefix $(HOST_PLATFORM)., $(BINS))) $(addprefix go.provider., $(addprefix $(HOST_PLATFORM)., $(PROVIDERS)))

.PHONY: go.build.all
go.build.all: go.build.verify $(foreach p,$(PLATFORMS),$(addprefix go.build., $(addprefix $(p)., $(BINS)))) $(foreach p,$(PLATFORMS),$(addprefix go.provider., $(addprefix $(p)., $(PROVIDERS))))


.PHONY: go.compress
go.compress: go.build.verify $(addprefix go.compress., $(addprefix $(HOST_PLATFORM)., $(BINS))) $(addprefix go.provider., $(addprefix $(HOST_PLATFORM)., $(PROVIDERS)))


.PHONY: go.compress.%
go.compress.%:
	$(eval COMMAND := $(word 2,$(subst ., ,$*)))
	$(eval PLATFORM := $(word 1,$(subst ., ,$*)))
	$(eval OS := $(word 1,$(subst _, ,$(PLATFORM))))
	$(eval ARCH := $(word 2,$(subst _, ,$(PLATFORM))))
	@echo "===========> Compressing binary $(COMMAND) $(VERSION) for $(OS) $(ARCH)"
	upx $(OUTPUT_DIR)/$(OS)/$(ARCH)/$(COMMAND)$(GO_OUT_EXT)

.PHONY: go.clean
go.clean:
	@echo "===========> Cleaning all build output"
	@rm -rf $(OUTPUT_DIR)

.PHONY: go.lint.verify
go.lint.verify: go.build.verify
ifeq (,$(wildcard $(TOOLS_DIR)/revive))
	@echo "===========> Installing revive"
	@mkdir -p $(TOOLS_DIR)
	@GOBIN=$(TOOLS_DIR) $(GO) install github.com/mgechev/revive
endif

.PHONY: go.lint
go.lint: go.lint.verify
	@echo "===========> Run revive to lint source codes"
	@$(TOOLS_DIR)/revive -config $(ROOT_DIR)/build/linter/revive.toml \
        -exclude vendor/... \
        ./...

.PHONY: go.test
go.test: go.build.verify
	@echo "===========> Run unit test"
	@$(GO) test -count=1 -timeout=10m -v ./...

.PHONY: go.test.cover
go.test.cover: go.build.verify
	@echo "===========> Run unit test coverage"
	@$(GO) test -count=1 -timeout=10m -cover -v ./... -json -coverprofile=coverage-all.out -covermode=count  > test.json