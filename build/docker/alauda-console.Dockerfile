FROM alpine:latest as builder

COPY . /workspace

RUN ARCH= && dpkgArch="$(arch)" \
  && case "${dpkgArch}" in \
    x86_64) ARCH='amd64';; \
    aarch64) ARCH='arm64';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  && cp /workspace/output/linux/$ARCH/alauda-console /bin/alauda-console && \
  /bin/alauda-console --help

FROM alpine:3.10

ENV TZ=Asia/Shanghai
ARG commit_id=dev
ARG app_version=dev

WORKDIR /console

CMD ["/console/alauda-console"]

ENTRYPOINT ["/bin/sh", "-c", "/console/alauda-console \"$0\" \"$@\""]

RUN apk add --no-cache curl

COPY --from=builder /bin/alauda-console /console/alauda-console

ENV COMMIT_ID=${commit_id}
ENV APP_VERSION=${app_version}
