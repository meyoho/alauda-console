FROM index.alauda.cn/alaudaorg/alaudabase-alpine-run:alpine3.9.3

ARG commit_id=dev
ARG app_version=dev

WORKDIR /files

CMD ["/files/copy.sh"]

ADD build/static/ /files

ENV COMMIT_ID=${commit_id}
ENV APP_VERSION=${app_version}
